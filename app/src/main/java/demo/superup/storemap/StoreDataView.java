package demo.superup.storemap;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import demo.superup.storemap.data.StoreDataItem;

/**
 * Created by alexy on 12.11.2015.
 */
public class StoreDataView extends RelativeLayout {
    protected StoreDataItem item;
    protected TextView storeName;
    protected TextView address;
    protected TextView city;
    protected TextView zip;
    protected View detailsView;
    protected DataViewListener listener;
    private DecelerateInterpolator interpolator = new DecelerateInterpolator();

    public StoreDataView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public StoreDataView(Context context) {
        super(context);
        init(context);
    }

    public StoreDataView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    protected void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.store_data, this);
        storeName = (TextView)findViewById(R.id.store_name);
        address = (TextView)findViewById(R.id.address);
        city = (TextView)findViewById(R.id.city);
        zip = (TextView)findViewById(R.id.zip);
        detailsView = findViewById(R.id.details_view);

//        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                setTranslationY(getHeight());
//                getViewTreeObserver().removeOnGlobalLayoutListener(this);
//            }
//        });

        setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeTop() {
                showDetails();
            }

            @Override
            public void onSwipeBottom() {
                if (getTranslationY() < storeName.getHeight()) {
                    hideDetails();
                } else {
                    hide();
                }
            }

            @Override
            public void onClick() {
                toggleDetails();
            }
        });
    }

    public void setItem(StoreDataItem item) {
        this.item = item;
        storeName.setText(item.getStoreName());
        address.setText(item.getAddress());
        city.setText(item.getCity());
        zip.setText(item.getZipCode());
    }

    public StoreDataItem getItem() {
        return item;
    }

    public void setListener(DataViewListener listener) {
        this.listener = listener;
    }


    public void hide() {
        Log.d("StoreDataView", "hide "+getTranslationY());
        Log.d("StoreDataView", "height " + getHeight());
        if (getHeight() == 0) {
            measure(MeasureSpec.UNSPECIFIED,MeasureSpec.UNSPECIFIED);
            setTranslationY(getMeasuredHeight());
        } else {
            animateTo(getHeight());
        }
    }

    public void show() {
        Log.d("StoreDataView", "show " + getTranslationY());
        Log.d("StoreDataView", "height "+getHeight());
        hideDetails();
    }

    public boolean isVisible() {
        Log.d("StoreDataView", "isVisible "+getTranslationY());
        Log.d("StoreDataView", "height "+getHeight());
        return getTranslationY() < getHeight();
    }

    public void showDetails() {
        Log.d("StoreDataView", "showDetails " + getTranslationY());
        Log.d("StoreDataView", "height "+getHeight());
//        setVisibility(VISIBLE);
        animateTo(0);
    }

    public void hideDetails() {
        Log.d("StoreDataView", "hideDetails "+getTranslationY());
        Log.d("StoreDataView", "height "+getHeight());
        animateTo(detailsView.getHeight());
    }

    public void toggleDetails() {
        if (getTranslationY() < detailsView.getHeight()) {
            hideDetails();
        } else {
            showDetails();
        }
    }

    private void animateTo(final int y) {
        animate().translationY(y).
                setDuration(getResources().getInteger(R.integer.anim_duration_short)).
                setInterpolator(interpolator).
                setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (y == getHeight()) {
                            Log.d("StoreDataView", "showDetails " + getTranslationY());
                            Log.d("StoreDataView", "height "+getHeight());

                            if (listener != null) {
                                listener.onHidden();
                            }
                        }
                    }
                });
    }

    public interface DataViewListener {
        void onHidden();
    }


}
