package demo.superup.storemap.data;

import android.os.AsyncTask;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import demo.superup.storemap.db.DbHelper;

/**
 * Created by alexy on 05.11.2015.
 */
public class ExternalStoresDataLoadTask extends AsyncTask<Void, Void, Void> {
    public interface StoreDataLoadListener {
        void onDataLoadingFinished();
        void onDataLoadingError(Throwable e);
    }

    private StoreDataLoadListener listener;
    private Reader reader;
    private DbHelper dbHelper;

    public ExternalStoresDataLoadTask(Reader reader, DbHelper dbHelper, StoreDataLoadListener listener) {
        this.listener = listener;
        this.reader = reader;
        this.dbHelper = dbHelper;
    }

    //StoreId;ChainId;SubChainId;StoreType;StoreName;Address;City;ZipCode;LastUpdateDate;LastUpdateTime;BikoretNo
    @Override
    protected Void doInBackground(Void... params) {
        ArrayList<StoreDataItem> items = new ArrayList<>();
        try {
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());

            for (CSVRecord record: csvParser) {
                StoreDataItem item = new StoreDataItem();
                item.setStoreId(record.get("StoreId"));
                item.setSubChainId(record.get("SubChainId"));
                item.setStoreType("1".equals(record.get("StoreType")));
                item.setStoreName(record.get("StoreName"));
                item.setAddress(record.get("Address"));
                item.setCity(record.get("City"));
                item.setZipCode(record.get("ZipCode"));

                String lastUpdateDate = record.get("LastUpdateDate");
                String lastUpdateTime = record.get("LastUpdateTime");

                if (!"NULL".equals(lastUpdateDate) && !"0000-00-00".equals(lastUpdateDate)) {
                    //todo parse data
                }

                item.setBikoretNo(Integer.getInteger(record.get("BikoretNo"), 0));

                try {
                    item.setLat(Double.valueOf(record.get("lat")));
                } catch (NumberFormatException e) {
                    item.setLat(0.0);
                }

                try {
                    item.setLon(Double.valueOf(record.get("lon")));
                } catch (NumberFormatException e) {
                    item.setLat(0.0);
                }

                items.add(item);
            }

            dbHelper.deleteAllItems();
            dbHelper.saveStoreItems(items);
        } catch (IOException e) {
            if (listener != null) {
                listener.onDataLoadingError(e);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (listener != null) {
            listener.onDataLoadingFinished();
        }
    }
}
