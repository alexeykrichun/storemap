package demo.superup.storemap.data;

import android.location.Location;
import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alexy on 04.11.2015.
 */
public class StoreDataItem implements Serializable {
    String storeId;
    String chainId;
    String subChainId;
    boolean storeType;
    String storeName;
    String address;
    String city;
    String zipCode;
    long lastUpdateTime;
    int bikoretNo;
    double lat;
    double lon;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getSubChainId() {
        return subChainId;
    }

    public void setSubChainId(String subChainId) {
        this.subChainId = subChainId;
    }

    public boolean isStoreTypeOffline() {
        return storeType;
    }

    public void setStoreType(boolean storeType) {
        this.storeType = storeType;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public int getBikoretNo() {
        return bikoretNo;
    }

    public void setBikoretNo(int bikoretNo) {
        this.bikoretNo = bikoretNo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getFullAddress() {
        ArrayList<String> addressFields = new ArrayList<>();
        if (!TextUtils.isEmpty(address)) {
            addressFields.add(address);
        }

        if (!TextUtils.isEmpty(city)) {
            addressFields.add(city);
        }

        if (!TextUtils.isEmpty(zipCode)) {
            addressFields.add(zipCode);
        }

        if (addressFields.size() > 0) {
            addressFields.add("ישראל");
            return TextUtils.join(", ", addressFields);
        }
        return null;
    }

    public Location getLocation() {
        Location location = new Location(storeName);
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }
}
