package demo.superup.storemap.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import demo.superup.storemap.R;
import demo.superup.storemap.data.StoreDataItem;

/**
 * Created by alexy on 06.11.2015.
 */
public class LocationUtils {
    public static Location getCurrentLocation(Context context) {
        android.location.LocationManager locationManager = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER) ||
                locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            return locationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER) ?
                    locationManager.getLastKnownLocation(android.location.LocationManager.NETWORK_PROVIDER) :
                    locationManager.getLastKnownLocation(android.location.LocationManager.GPS_PROVIDER);
        }
        return null;
    }

    public static BitmapDescriptor getBitmapDescriptor(int id, Context context) {
        Drawable vectorDrawable = context.getDrawable(id);
        if (vectorDrawable != null) {
            int size = context.getResources().getDimensionPixelSize(R.dimen.vector_drawable_size);
            vectorDrawable.setBounds(0, 0, size, size);
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            vectorDrawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }
        return null;
    }

    public static void startNavigation(Context context, StoreDataItem item, LatLng start) {
        String uri = "http://maps.google.com/maps?f=d&hl=en&saddr=" +
                start.latitude + "," + start.longitude +
                "&daddr=" + item.getLat() + "," + item.getLon();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(Intent.createChooser(intent, "Select an application"));
    }
}
