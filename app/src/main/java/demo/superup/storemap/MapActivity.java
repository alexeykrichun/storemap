package demo.superup.storemap;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.transition.Slide;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import demo.superup.storemap.data.ExternalStoresDataLoadTask;
import demo.superup.storemap.data.StoreDataItem;
import demo.superup.storemap.db.DbLoadStoresTask;
import demo.superup.storemap.util.LatLngInterpolator;
import demo.superup.storemap.util.LocationUtils;
import demo.superup.storemap.util.MarkerAnimation;

public class MapActivity extends BaseStoreActivity implements OnMapReadyCallback {
    protected final String LOG_TAG = "MapActivity";

    private GoogleMap googleMap;
    private ImageButton listButton;
    private FloatingActionButton buttonNavigate;

    private boolean externalDataLoaded = false;
    private StoreDataView storeDataView;
    private HashMap<Marker, StoreDataItem> markerHashMap;

    private BitmapDescriptor storeMarkerImage;
    private BitmapDescriptor selectedStoreMarkerImage;
    private BitmapDescriptor currentLocationImage;

    private Marker selectedStoreMarker;
    private Marker currentLocationMarker;

    private LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Linear();

    private DbLoadStoresTask.DbLoadStoresTaskListener dbLoadStoresTaskListener = new DbLoadStoresTask.DbLoadStoresTaskListener() {
        @Override
        public void storeDataLoaded(ArrayList<StoreDataItem> items) {
            storeItems = items;
            showStoresOnMap();
            progressBar.setVisibility(View.INVISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.LEFT);
        slide.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        getWindow().setEnterTransition(slide);
        getWindow().setReenterTransition(slide);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        listButton = (ImageButton)findViewById(R.id.button_list);
        storeDataView = (StoreDataView)findViewById(R.id.store_data);
        storeDataView.setListener(new StoreDataView.DataViewListener() {
            @Override
            public void onHidden() {
                buttonNavigate.hide();
                if (selectedStoreMarker != null) {
                    selectedStoreMarker.setIcon(storeMarkerImage);
                    selectedStoreMarker = null;
                }
            }
        });

        storeDataView.hide();

        buttonNavigate = (FloatingActionButton)findViewById(R.id.button_navigate);
        buttonNavigate.hide();

        buttonNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storeDataView.getItem() != null && currentLocation != null) {
                    LocationUtils.startNavigation(MapActivity.this, storeDataView.getItem(), currentLocation);
                }
            }
        });

        mapFragment.getMapAsync(this);
        markerHashMap = new HashMap<>();

        try {
            Reader dataReader = new InputStreamReader(getAssets().open("Stores-coords.csv"));
            new ExternalStoresDataLoadTask(dataReader, dbHelper, new ExternalStoresDataLoadTask.StoreDataLoadListener() {
                @Override
                public void onDataLoadingFinished() {
                    externalDataLoaded = true;
                    completeStartup();
                }

                @Override
                public void onDataLoadingError(Throwable e) {
                    showError(getString(R.string.data_loading_error));
                }
            }).execute();
        } catch (IOException e) {
            showError(getString(R.string.data_loading_error));
        }

        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ActivityOptions activityOptions = ActivityOptions.makeScaleUpAnimation(listButton, 0, 0, listButton.getWidth(), listButton.getHeight());

                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(MapActivity.this, true);
                ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(MapActivity.this, pairs);
                showStoreList(activityOptions);
            }
        });
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(googleMap == null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            super.onNewIntent(intent);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_map;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.setTrafficEnabled(false);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);

        int padding = getResources().getDimensionPixelSize(R.dimen.activity_margin);
        this.googleMap.setPadding(padding, padding, padding, padding);

        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                StoreDataItem item = markerHashMap.get(marker);
                if (item != null) {
                    storeDataView.setItem(item);
                    storeDataView.show();
                    buttonNavigate.show();
                    if (selectedStoreMarker != null) {
                        selectedStoreMarker.setIcon(storeMarkerImage);
                    }
                    selectedStoreMarker = marker;
                    selectedStoreMarker.setIcon(selectedStoreMarkerImage);
                }
                return true;
            }
        });

        this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (selectedStoreMarker != null) {
                    selectedStoreMarker.setIcon(storeMarkerImage);
                    selectedStoreMarker = null;
                }
                storeDataView.hide();
                buttonNavigate.hide();
            }
        });

        storeMarkerImage = LocationUtils.getBitmapDescriptor(R.drawable.map_marker, this);
        currentLocationImage = LocationUtils.getBitmapDescriptor(R.drawable.crosshairs_gps, this);
        selectedStoreMarkerImage = LocationUtils.getBitmapDescriptor(R.drawable.map_marker_selected, this);

        completeStartup();
    }

    private void completeStartup() {
        if (externalDataLoaded && googleMap != null) {
            onPostInit();
        }
    }

    @Override
    protected void loadStores() {
        if (currentLocation != null) {
            if (currentLocationMarker != null) {
                MarkerAnimation.animateMarker(currentLocationMarker, currentLocation, latLngInterpolator, 250);
            } else {
                currentLocationMarker = googleMap.addMarker(new MarkerOptions().position(currentLocation).
                        icon(currentLocationImage).anchor(0.5f, 0.5f));
            }

            progressBar.setVisibility(View.VISIBLE);
            clearStores();

            new DbLoadStoresTask(currentLocation, distanceSeekBar.getValue(), dbHelper, dbLoadStoresTaskListener).execute();
        }
    }

    private void showStoresOnMap() {

        if (storeItems != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(currentLocation);

            for(StoreDataItem item:storeItems) {
                LatLng coords = new LatLng(item.getLat(), item.getLon());
                builder.include(coords);

                Marker marker = googleMap.addMarker(new MarkerOptions().position(coords).icon(storeMarkerImage)
                        .title(item.getStoreName()));
                markerHashMap.put(marker, item);
            }
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 0));

        }
    }

    @Override
    protected void clearStores() {
        Iterator<Marker> iterator = markerHashMap.keySet().iterator();
        while (iterator.hasNext()) {
            iterator.next().remove();
            iterator.remove();
        }
        selectedStoreMarker = null;
    }

    @Override
    public void onBackPressed() {
        if (storeDataView.isVisible()) {
            storeDataView.hide();
            buttonNavigate.hide();
            if (selectedStoreMarker != null) {
                selectedStoreMarker.setIcon(storeMarkerImage);
                selectedStoreMarker = null;
            }
        } else {
            super.onBackPressed();
        }
    }
}
