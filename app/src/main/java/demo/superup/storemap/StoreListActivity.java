package demo.superup.storemap;

import android.app.ActivityOptions;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.transition.Explode;
import android.transition.Slide;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.Toolbar;

import java.util.ArrayList;
import java.util.Locale;

import demo.superup.storemap.data.StoreDataItem;
import demo.superup.storemap.db.DbHelper;
import demo.superup.storemap.db.DbLoadStoresTask;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;


public class StoreListActivity extends BaseStoreActivity {
    private RecyclerView storeList;
    private View noStores;
    private ImageButton mapButton;
    private boolean first = true;

    private DbLoadStoresTask.DbLoadStoresTaskListener dbLoadStoresTaskListener = new DbLoadStoresTask.DbLoadStoresTaskListener() {
        @Override
        public void storeDataLoaded(ArrayList<StoreDataItem> items) {
            progressBar.setVisibility(View.INVISIBLE);
            if (storeList.getAdapter() == null) {
                StoreListAdapter adapter = new StoreListAdapter(StoreListActivity.this, items, currentLocation);
                storeList.setAdapter(adapter);
            } else {
                if (storeList.getAdapter() instanceof StoreListAdapter) {
                    ((StoreListAdapter) storeList.getAdapter()).setItems(items);
                }
            }

            if (items == null || items.size() == 0) {
                storeList.setVisibility(View.GONE);
                noStores.setVisibility(View.VISIBLE);
            } else {
                storeList.setVisibility(View.VISIBLE);
                noStores.setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Slide slide = new Slide();
        slide.setSlideEdge(TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) == View.LAYOUT_DIRECTION_LTR ?
                Gravity.LEFT : Gravity.RIGHT);
        slide.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        getWindow().setEnterTransition(slide);

        Explode explode = new Explode();
        explode.setDuration(getResources().getInteger(R.integer.anim_duration_short));
        explode.setInterpolator(new DecelerateInterpolator());
        getWindow().setReenterTransition(explode);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);
        storeList = (RecyclerView)findViewById(R.id.store_list);
        storeList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        storeList.setItemAnimator(new SlideInUpAnimator());
        storeList.setHasFixedSize(true);

        noStores = findViewById(R.id.no_stores);
        noStores.setVisibility(View.GONE);

        setView(getIntent());

        mapButton = (ImageButton)findViewById(R.id.button_map);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStoreMap();
            }
        });

        onPostInit();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_store_list;
    }

    @Override
    protected void clearStores() {

    }

    protected void loadStores() {
        progressBar.setVisibility(View.VISIBLE);
        if (first) {
            first = false;
            storeList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadStores();
                }
            }, getResources().getInteger(R.integer.anim_duration_long));
            return;
        }
        new DbLoadStoresTask(currentLocation,
                distanceSeekBar.getValue(),
                DbHelper.getInstance(this),
                dbLoadStoresTaskListener).execute();
    }

    @Override
    public void onBackPressed() {
        showStoreMap();
    }

    private void showStoreMap() {
//        ActivityOptions activityOptions = ActivityOptions.makeScaleUpAnimation(mapButton, 0, 0, mapButton.getWidth(), mapButton.getHeight());

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(StoreListActivity.this, true);
        ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(StoreListActivity.this, pairs);

        showStoreMap(activityOptions);
    }
}
