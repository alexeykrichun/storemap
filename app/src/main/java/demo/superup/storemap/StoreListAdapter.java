package demo.superup.storemap;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import demo.superup.storemap.data.StoreDataItem;

/**
 * Created by alexy on 10.11.2015.
 */
public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.ViewHolder> {

    private List<StoreDataItem> items;
    private LatLng location;
    private Activity activity;

    private int lastPosition = -1;
    private DecelerateInterpolator interpolator = new DecelerateInterpolator();

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView storeName;
        TextView storeAddress;
        TextView distance;

        public ViewHolder(View itemView, TextView storeName, TextView storeAddress, TextView distance) {
            super(itemView);
            this.storeName = storeName;
            this.storeAddress = storeAddress;
            this.distance = distance;
        }
    }

    public StoreListAdapter(Activity activity, List<StoreDataItem> items, LatLng location) {
        setItems(items);
        this.location = location;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_list_item, parent, false);
        TextView storeName = (TextView)view.findViewById(R.id.store_name);
        TextView storeAddress = (TextView)view.findViewById(R.id.store_address);
        TextView distance = (TextView)view.findViewById(R.id.distance);

        return new ViewHolder(view, storeName, storeAddress, distance);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        runEnterAnimation(holder.itemView, position);

        if (items != null && items.size() > position) {
            final StoreDataItem item = items.get(position);
            if (item != null) {
                holder.storeName.setText(item.getStoreName());
                holder.storeAddress.setText(item.getFullAddress());
                if (location != null) {
                    Location location1 = new Location(item.getStoreName());
                    location1.setLatitude(location.latitude);
                    location1.setLongitude(location.longitude);
                    float distance = location1.distanceTo(item.getLocation()) / 1000f;
                    holder.distance.setText(activity.getString(R.string.distance_format, distance));
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, StoreDetailsActivity.class);
                    intent.putExtra(StoreDetailsActivity.EXTRA_STORE_DATA, item);
                    intent.putExtra(StoreDetailsActivity.EXTRA_LOCATION, location);
                    Pair<View, String> titlePair = new Pair<View, String>(holder.itemView, activity.getString(R.string.transition_name));
                    ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(activity, titlePair);
                    activity.startActivity(intent, activityOptions.toBundle());
                }
            });
        }

    }

    private void runEnterAnimation(View view, int position) {

        if (position > lastPosition) {
            lastPosition = position;
            view.setTranslationY(view.getHeight() * 3);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(20 * (position))
                    .setInterpolator(interpolator)
                    .setDuration(activity.getResources().getInteger(R.integer.anim_duration_short))
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        }
        return 0;
    }

    public void setLocation(LatLng location) {
        this.location = location;
        notifyItemRangeChanged(0, getItemCount());
    }

    public void setItems(List<StoreDataItem> items) {
        lastPosition = -1;
        this.items = items;
        notifyDataSetChanged();
    }
}
