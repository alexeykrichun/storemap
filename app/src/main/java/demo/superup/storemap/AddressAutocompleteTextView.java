package demo.superup.storemap;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by alexy on 15.11.2015.
 */
public class AddressAutocompleteTextView extends ClearableAutoCompleteTextView {
    private static final String LOG_TAG = "AddressAutocomplete";

    private final static LatLngBounds BOUNDS_ISRAEL = new LatLngBounds(new LatLng(29.4965, 34.2677), new LatLng(33.43338, 35.940941));

    private PlaceArrayAdapter adapter;
    private ResultCallback<PlaceBuffer> resultCallback;
    private GoogleApiClient googleApiClient;

    private AdapterView.OnItemClickListener autocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = adapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, placeId);
            placeResult.setResultCallback(resultCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    public AddressAutocompleteTextView(Context context) {
        super(context);
        init(context);
    }

    public AddressAutocompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AddressAutocompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        adapter = new PlaceArrayAdapter(getContext(), android.R.layout.simple_list_item_1, BOUNDS_ISRAEL, null);
        setAdapter(adapter);
        setOnItemClickListener(autocompleteClickListener);
    }

    public void setResultCallback(ResultCallback<PlaceBuffer> resultCallback) {
        this.resultCallback = resultCallback;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        adapter.setGoogleApiClient(googleApiClient);
        this.googleApiClient = googleApiClient;
    }
}
