package demo.superup.storemap;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import demo.superup.storemap.data.StoreDataItem;
import demo.superup.storemap.util.LocationUtils;

public class StoreDetailsActivity extends Activity {
    public final static String EXTRA_STORE_DATA="StoreDetailsActivity.storeData";
    public final static String EXTRA_LOCATION="StoreDetailsActivity.location";

    private StoreDataItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        setContentView(R.layout.activity_store_details);

        Transition  transition = TransitionInflater.from(this).inflateTransition(R.transition.store_details);
        getWindow().setEnterTransition(transition);

        item = (StoreDataItem)getIntent().getSerializableExtra(EXTRA_STORE_DATA);
        if (item != null) {
            TextView title = (TextView)findViewById(R.id.title);
            TextView address = (TextView)findViewById(R.id.address);
            TextView city = (TextView)findViewById(R.id.city);
            TextView zip = (TextView)findViewById(R.id.zip);
            title.setText(item.getStoreName());
            address.setText(item.getAddress());
            city.setText(item.getCity());
            zip.setText(item.getZipCode());
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng location = (LatLng)getIntent().getParcelableExtra(EXTRA_LOCATION);
                LocationUtils.startNavigation(StoreDetailsActivity.this, item, location);
            }
        });
    }

}
