package demo.superup.storemap;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by alexy on 06.11.2015.
 */
public class ValueSeekBar extends LinearLayout {
    private SeekBar seekBar;
    private TextView value;

    private final int VALUE_MULTIPLIER = 5;

    interface ValueSeekBarListener{
        void onValueChanged(int value);
    }

    private ValueSeekBarListener listener;

    public ValueSeekBar(Context context) {
        super(context);
        init(context);
    }

    public ValueSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ValueSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ValueSeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void setListener(ValueSeekBarListener listener) {
        this.listener = listener;
    }

    void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.value_seek_bar, this);
        seekBar = (SeekBar)findViewById(R.id.seek_bar);
        value = (TextView)findViewById(R.id.value);
        value.setText(String.valueOf(getValue()));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value.setText(String.valueOf(getValue()));
                if (listener != null) {
                    listener.onValueChanged(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public int getValue() {
        return seekBar.getProgress() * VALUE_MULTIPLIER;
    }

    public void setValue(int value) {
        seekBar.setProgress(value / VALUE_MULTIPLIER);
    }
}
