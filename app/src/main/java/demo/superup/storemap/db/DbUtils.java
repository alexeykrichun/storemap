package demo.superup.storemap.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbUtils {

    public static int UNDEFINED = -1;
    private static String LOG_TAG = "DbUtils";

    public static String safeGetString(Cursor c, String columnName) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0) {
                return null;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return null;
            }
            return c.getString(colIdx);
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
            return null;
        }

    }

    public static int safeGetInt(Cursor c, String columnName, int defaultValue) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0) {
                return defaultValue;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return defaultValue;
            }
            return c.getInt(colIdx);
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
            return defaultValue;
        }

    }

    public static long safeGetLong(Cursor c, String columnName, long defaultValue) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0) {
                return defaultValue;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return defaultValue;
            }
            return c.getLong(colIdx);
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
            return defaultValue;
        }
    }

    public static byte[] safeGetBlob(Cursor c, String columnName) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0) {
                return null;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return null;
            }
            return c.getBlob(colIdx);
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
            return null;
        }

    }

    public static double safeGetDouble(Cursor c, String columnName, double defaultValue) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0) {
                return defaultValue;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return defaultValue;
            }
            return c.getDouble(colIdx);
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
            return defaultValue;
        }
    }

    public static float safeGetFloat(Cursor c, String columnName, float defaultValue) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0) {
                return defaultValue;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return defaultValue;
            }
            return c.getFloat(colIdx);
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
            return defaultValue;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Enum<T>> T safeGetEnum(Cursor c, String columnName, Enum<T> defaultValue) {
        try {
            if (c == null || columnName == null || c.isClosed() || c.getCount() <= 0 || defaultValue == null) {
                return (T) defaultValue;
            }
            int colIdx = c.getColumnIndex(columnName);
            if (colIdx < 0 || colIdx >= c.getColumnCount()) {
                return (T) defaultValue;
            }
            Enum<T>[] values = defaultValue.getClass().getEnumConstants();
            int ordinal = c.getInt(colIdx);
            if (ordinal >= 0 && ordinal < values.length) {
                return (T) values[ordinal];
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
        }
        return (T) defaultValue;
    }

    public static void safeCloseCursor(Cursor c) {
        try {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
        }
    }

    public static void safeCloseDb(SQLiteDatabase db) {
        try {
            if (db != null && db.isOpen()) {
                db.close();
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "DB error", e);
        }
    }

    public static void dumpCursor(Cursor c, String entryLogTag, boolean printBlob) {
        Log.d(LOG_TAG, "dump["+entryLogTag+"]s");
        if (c != null) {
            Log.d(LOG_TAG, "count= " + c.getCount());
            while (c.moveToNext()) {
                String[] colnames = c.getColumnNames();
                StringBuilder strB = new StringBuilder();
                strB.append("* ").append(entryLogTag);
                int idx = 0;
                for (String colname : colnames) {
                    strB.append(colname).append('=');
                    switch (c.getType(idx)) {
                        case Cursor.FIELD_TYPE_BLOB:
                            strB.append(printBlob ? new String(safeGetBlob(c, colname)) : "BLOB...");
                            break;
                        case Cursor.FIELD_TYPE_FLOAT:
                            strB.append(safeGetFloat(c, colname, -1));
                            break;
                        case Cursor.FIELD_TYPE_INTEGER:
                            strB.append(safeGetLong(c, colname, -1));
                            break;
                        case Cursor.FIELD_TYPE_NULL:
                            strB.append("NULL");
                            break;
                        case Cursor.FIELD_TYPE_STRING:
                            strB.append(safeGetString(c, colname));
                            break;
                    }
                    idx++;
                    strB.append(", ");
                }
                Log.d(LOG_TAG, strB.toString());
            }
        } else {
            Log.d(LOG_TAG, "NULL cursor");
        }
    }
}
