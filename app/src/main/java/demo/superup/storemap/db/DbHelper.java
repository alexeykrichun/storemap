package demo.superup.storemap.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import demo.superup.storemap.data.StoreDataItem;

/**
 * Created by alexy on 04.11.2015.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "superup.demo";
    private static final int DATABASE_VERSION = 1;

    private static DbHelper instance;

    private DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DbHelper(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + StoreTable.TABLE_NAME + " ( " +
                StoreTable._ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE`, " +
                StoreTable.COLUMN_ADDRESS + " TEXT, " +
                StoreTable.COLUMN_BIKORET + " INTEGER DEFAULT 0, " +
                StoreTable.COLUMN_CHAIN_ID + " TEXT, " +
                StoreTable.COLUMN_CITY + " TEXT, " +
                StoreTable.COLUMN_LAST_UPDATE + " INTEGER DEFAULT -1, " +
                StoreTable.COLUMN_LAT + " REAL DEFAULT 0, " +
                StoreTable.COLUMN_LON + " REAL DEFAULT 0, " +
                StoreTable.COLUMN_STORE_ID + " TEXT, " +
                StoreTable.COLUMN_STORE_NAME + " TEXT, " +
                StoreTable.COLUMN_STORE_TYPE + " INTEGER DEFAULT 0, " +
                StoreTable.COLUMN_SUBCHAIN_ID + " TEXT, " +
                StoreTable.COLUMN_ZIPCODE + " TEXT, " +
                StoreTable.COLUMN_LAT_SIN + " REAL, " +
                StoreTable.COLUMN_LAT_COS + " REAL, " +
                StoreTable.COLUMN_LON_SIN + " REAL, " +
                StoreTable.COLUMN_LON_COS + " REAL, " +
                "UNIQUE (" + StoreTable.COLUMN_CHAIN_ID + "," + StoreTable.COLUMN_STORE_ID + " ) ON CONFLICT REPLACE )  ;");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + StoreTable.TABLE_NAME);
        onCreate(db);
    }

    public void saveStoreItem(StoreDataItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getContentValues(item);
        db.insert(StoreTable.TABLE_NAME, null, values);
        db.close();
    }

    public void saveStoreItems(List<StoreDataItem> items) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (StoreDataItem item: items) {
                ContentValues values = getContentValues(item);
                db.insert(StoreTable.TABLE_NAME, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public StoreDataItem getItem(String chainId, String storeId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return getItem((chainId+storeId).hashCode());
    }

    public StoreDataItem getItem(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(StoreTable.TABLE_NAME,
                StoreTable.PROJECTION_ALL,
                StoreTable._ID + " =? ",
                new String[]{String.valueOf(id)},
                null, null, null, "1");
        if (cursor.moveToFirst()) {
            return getStoreItem(cursor);
        }
        return null;
    }

    public ArrayList<StoreDataItem> getAllItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<StoreDataItem> items = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + StoreTable.TABLE_NAME, null);
        if (cursor.moveToFirst()) {
            do {
                items.add(getStoreItem(cursor));
            } while (cursor.moveToNext());
        }
        return items;
    }

    public ArrayList<StoreDataItem> getItemsInRadius(LatLng center, int raduisKM) {
        final double cosRadius = Math.cos(raduisKM / 6371.0);
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<StoreDataItem> items = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT *, "+buildDistanceQuery(center.latitude, center.longitude)+
                " AS DISTANCE FROM " + StoreTable.TABLE_NAME + " WHERE DISTANCE >= " + cosRadius + " ORDER BY DISTANCE DESC"
                , null);

        if (cursor.moveToFirst()) {
            do {
                items.add(getStoreItem(cursor));
            } while (cursor.moveToNext());
        }
        return items;

    }

    public static String buildDistanceQuery(double latitude, double longitude) {
        final double cosLat = Math.cos(Math.toRadians(latitude));
        final double sinLat = Math.sin(Math.toRadians(latitude));
        final double cosLon = Math.cos(Math.toRadians(longitude));
        final double sinLon = Math.sin(Math.toRadians(longitude));

        // d = acos(sin(lat1)*sin(lat2)+ cos(lat1)*cos(lat2)*(cos(long2)*cos(long1)+sin(long2)*sin(long1))) * R

        return "(" + sinLat + "*" + StoreTable.COLUMN_LAT_SIN + "+" +
                cosLat + "*" + StoreTable.COLUMN_LAT_COS +
                "*(" + cosLon + "*" + StoreTable.COLUMN_LON_COS + "+" +
                sinLon + "*" + StoreTable.COLUMN_LON_SIN + "))";
    }

    public void deleteAllItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+StoreTable.TABLE_NAME);
        db.close();
    }


    @NonNull
    private static ContentValues getContentValues(StoreDataItem item) {
        ContentValues values = new ContentValues();
        values.put(StoreTable._ID, getStoreUniqueId(item));
        values.put(StoreTable.COLUMN_ADDRESS, item.getAddress());
        values.put(StoreTable.COLUMN_BIKORET, item.getBikoretNo());
        values.put(StoreTable.COLUMN_CHAIN_ID, item.getChainId());
        values.put(StoreTable.COLUMN_CITY, item.getCity());
        values.put(StoreTable.COLUMN_LAST_UPDATE, item.getLastUpdateTime());
        values.put(StoreTable.COLUMN_LAT, item.getLat());
        values.put(StoreTable.COLUMN_LON, item.getLon());
        values.put(StoreTable.COLUMN_STORE_ID, item.getStoreId());
        values.put(StoreTable.COLUMN_STORE_NAME, item.getStoreName());
        values.put(StoreTable.COLUMN_STORE_TYPE, item.isStoreTypeOffline() ? 1 : 0);
        values.put(StoreTable.COLUMN_SUBCHAIN_ID, item.getSubChainId());
        values.put(StoreTable.COLUMN_ZIPCODE, item.getZipCode());
        values.put(StoreTable.COLUMN_LAT_SIN, Math.sin(Math.toRadians(item.getLat())));
        values.put(StoreTable.COLUMN_LAT_COS, Math.cos(Math.toRadians(item.getLat())));
        values.put(StoreTable.COLUMN_LON_SIN, Math.sin(Math.toRadians(item.getLon())));
        values.put(StoreTable.COLUMN_LON_COS, Math.cos(Math.toRadians(item.getLon())));
        return values;
    }

    public static StoreDataItem getStoreItem(Cursor cursor) {
        StoreDataItem item = new StoreDataItem();
        item.setAddress(DbUtils.safeGetString(cursor, StoreTable.COLUMN_ADDRESS));
        item.setBikoretNo(DbUtils.safeGetInt(cursor, StoreTable.COLUMN_BIKORET, -1));
        item.setChainId(DbUtils.safeGetString(cursor, StoreTable.COLUMN_CHAIN_ID));
        item.setCity(DbUtils.safeGetString(cursor, StoreTable.COLUMN_CITY));
        item.setLastUpdateTime(DbUtils.safeGetLong(cursor, StoreTable.COLUMN_LAST_UPDATE, -1L));
        item.setLat(DbUtils.safeGetDouble(cursor, StoreTable.COLUMN_LAT, 0.0));
        item.setLon(DbUtils.safeGetDouble(cursor, StoreTable.COLUMN_LON, 0.0));
        item.setStoreId(DbUtils.safeGetString(cursor, StoreTable.COLUMN_STORE_ID));
        item.setStoreName(DbUtils.safeGetString(cursor, StoreTable.COLUMN_STORE_NAME));
        item.setStoreType(DbUtils.safeGetInt(cursor, StoreTable.COLUMN_STORE_TYPE, 0) == 1);
        item.setSubChainId(DbUtils.safeGetString(cursor, StoreTable.COLUMN_SUBCHAIN_ID));
        item.setZipCode(DbUtils.safeGetString(cursor, StoreTable.COLUMN_ZIPCODE));
        return item;
    }

    private static int getStoreUniqueId(StoreDataItem item) {
        return (item.getChainId() + item.getStoreId()).hashCode();
    }

}
