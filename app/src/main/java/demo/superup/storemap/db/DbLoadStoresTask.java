package demo.superup.storemap.db;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import demo.superup.storemap.data.StoreDataItem;

/**
 * Created by alexy on 06.11.2015.
 */
public class DbLoadStoresTask extends AsyncTask<Void, Void, ArrayList<StoreDataItem>> {
    private LatLng center;
    private int radius;
    private DbHelper dbHelper;

    public interface DbLoadStoresTaskListener{
        void storeDataLoaded(ArrayList<StoreDataItem> items);
    }
    private DbLoadStoresTaskListener listener;

    public DbLoadStoresTask(LatLng center, int radius, DbHelper dbHelper, DbLoadStoresTaskListener listener) {
        this.center = center;
        this.radius = radius;
        this.listener = listener;
        this.dbHelper = dbHelper;
    }

    @Override
    protected ArrayList<StoreDataItem> doInBackground(Void... params) {
        if (center == null || radius == 0) {
            return null;
        }

        return dbHelper.getItemsInRadius(center, radius);
    }

    @Override
    protected void onPostExecute(ArrayList<StoreDataItem> storeDataItems) {
        if (listener != null) {
            listener.storeDataLoaded(storeDataItems);
        }
    }
}
