package demo.superup.storemap.db;

import android.provider.BaseColumns;

/**
 * Created by alexy on 04.11.2015.
 */
public interface StoreTable extends BaseColumns {
    String TABLE_NAME = "stores";

    String COLUMN_STORE_ID = "storeId";
    String COLUMN_CHAIN_ID = "chainId";
    String COLUMN_SUBCHAIN_ID = "subChainId";
    String COLUMN_STORE_TYPE = "storeType";
    String COLUMN_STORE_NAME = "storeName";
    String COLUMN_ADDRESS = "address";
    String COLUMN_CITY = "city";
    String COLUMN_ZIPCODE = "zipCode";
    String COLUMN_LAST_UPDATE = "lastUpdate";
    String COLUMN_BIKORET = "bikoret";
    String COLUMN_LAT = "lat";
    String COLUMN_LON = "lon";
    String COLUMN_LAT_SIN = "latSin";
    String COLUMN_LAT_COS = "latCos";
    String COLUMN_LON_SIN = "lonSin";
    String COLUMN_LON_COS = "lonCos";


    String[] PROJECTION_ALL = {
            COLUMN_ADDRESS,
            COLUMN_BIKORET,
            COLUMN_CHAIN_ID,
            COLUMN_CITY,
            COLUMN_LAST_UPDATE,
            COLUMN_LON,
            COLUMN_LAT,
            COLUMN_STORE_ID,
            COLUMN_STORE_NAME,
            COLUMN_STORE_TYPE,
            COLUMN_SUBCHAIN_ID,
            COLUMN_ZIPCODE,
            COLUMN_LAT_SIN,
            COLUMN_LAT_COS,
            COLUMN_LON_SIN,
            COLUMN_LON_COS
    };
}
