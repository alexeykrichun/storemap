package demo.superup.storemap;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import demo.superup.storemap.data.StoreDataItem;
import demo.superup.storemap.db.DbHelper;

/**
 * Created by alexy on 18.11.2015.
 */
public abstract class BaseStoreActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String EXTRA_LOCATION = "BaseStoreActivity.Location";
    public static final String EXTRA_ADDRESS = "BaseStoreActivity.Address";
    public static final String EXTRA_DISTANCE = "BaseStoreActivity.Distance";

    private static final int REQUEST_CODE_LIST_ACTIVITY = 2000;
    private static final int REQUEST_CODE_MAP_ACTIVITY = 2001;

    protected final String LOG_TAG = "BaseStoreActivity";
    protected ArrayList<StoreDataItem> storeItems;
    protected ValueSeekBar distanceSeekBar;
    protected ImageButton locationButton;
    protected AddressAutocompleteTextView addressTextView;
    protected ProgressBar progressBar;

    protected DbHelper dbHelper;
    protected LatLng currentLocation;
    protected GoogleApiClient googleApiClient;

    private ResultCallback<PlaceBuffer> placeResultCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            addressTextView.setCursorVisible(false);
            if (!places.getStatus().isSuccess()) {
                showError(getString(R.string.places_error));
                Log.e(LOG_TAG, "Places error " + places.getStatus().toString());
                progressBar.setVisibility(View.INVISIBLE);
                return;
            }
            Place place = places.get(0);
            if (place != null) {
                currentLocation = place.getLatLng();
                loadStores();
            }
            places.release();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        dbHelper = DbHelper.getInstance(this);

        locationButton = (ImageButton)findViewById(R.id.button_location);
        addressTextView = (AddressAutocompleteTextView)findViewById(R.id.address_autocomplete);
        addressTextView.setThreshold(3);
        distanceSeekBar = (ValueSeekBar)findViewById(R.id.distance_seek_bar);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        addressTextView.setClearButton(getDrawable(R.drawable.close));
        addressTextView.setEnabled(false);
        addressTextView.setCursorVisible(false);
        addressTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressTextView.setCursorVisible(true);
            }
        });

        locationButton = (ImageButton)findViewById(R.id.button_location);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    protected void onPostInit() {
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCurrentLocation();
            }
        });

        distanceSeekBar.setListener(new ValueSeekBar.ValueSeekBarListener() {
            @Override
            public void onValueChanged(int value) {
                loadStores();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        setView(intent);
        loadStores();
    }

    protected void setView(Intent intent) {
        currentLocation = (LatLng)intent.getParcelableExtra(EXTRA_LOCATION);
        String address = intent.getStringExtra(EXTRA_ADDRESS);
        addressTextView.setText(address, false);
        int distance = intent.getIntExtra(EXTRA_DISTANCE, 50);
        distanceSeekBar.setValue(distance);
    }

    protected void showStoreList(ActivityOptions activityOptions) {
        Intent intent = new Intent(BaseStoreActivity.this, StoreListActivity.class);
        intent.putExtra(StoreListActivity.EXTRA_LOCATION, currentLocation);
        intent.putExtra(StoreListActivity.EXTRA_DISTANCE, distanceSeekBar.getValue());
        intent.putExtra(StoreListActivity.EXTRA_ADDRESS, addressTextView.getText().toString());
        startActivity(intent, activityOptions.toBundle());
    }

    protected void showStoreMap(ActivityOptions activityOptions) {
        Intent intent = new Intent(BaseStoreActivity.this, MapActivity.class);
        intent.putExtra(StoreListActivity.EXTRA_LOCATION, currentLocation);
        intent.putExtra(StoreListActivity.EXTRA_DISTANCE, distanceSeekBar.getValue());
        intent.putExtra(StoreListActivity.EXTRA_ADDRESS, addressTextView.getText().toString());
        startActivity(intent, activityOptions.toBundle());
    }


    protected abstract int getLayout();

    protected abstract void loadStores();

    protected abstract void clearStores();

    @Override
    public void onConnected(Bundle bundle) {
        addressTextView.setGoogleApiClient(googleApiClient);
        addressTextView.setResultCallback(placeResultCallback);

        if (currentLocation == null) {
            selectCurrentLocation();
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            addressTextView.setEnabled(true);
            loadStores();
        }
    }

    protected void selectCurrentLocation() {
        progressBar.setVisibility(View.VISIBLE);
        addressTextView.setEnabled(false);
        PendingResult<PlaceLikelihoodBuffer> currentPlace = Places.PlaceDetectionApi.getCurrentPlace(googleApiClient, null);
        currentPlace.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer placeLikelihoods) {
                if (!placeLikelihoods.getStatus().isSuccess()) {
                    showError(getString(R.string.places_error));
                    Log.e(LOG_TAG, "Places error " + placeLikelihoods.getStatus().toString());
                    addressTextView.setEnabled(true);
                    progressBar.setVisibility(View.INVISIBLE);
                    return;
                }
                if (placeLikelihoods.get(0) != null) {
                    Place current = placeLikelihoods.get(0).getPlace();
                    addressTextView.setText(current.getAddress(), false);
                    currentLocation = current.getLatLng();
                    loadStores();
                    addressTextView.setEnabled(true);
                }
                placeLikelihoods.release();
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        addressTextView.setGoogleApiClient(null);

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showError(getString(R.string.google_connection_error), getString(R.string.retry), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleApiClient.connect();
            }
        });
        Log.e(LOG_TAG, "Google Places API connection failed with error code: " + connectionResult.getErrorCode());

    }

    protected void showError(String message) {
        showError(message, getString(android.R.string.ok), null);
    }

    protected void showError(String message, String action, View.OnClickListener listener) {
        final Snackbar snackbar =  Snackbar.make(findViewById(R.id.activity_layout), message, Snackbar.LENGTH_INDEFINITE);
        if (listener == null) {
            listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            };
        }
        snackbar.setAction(action, listener);
        snackbar.show();

    }
}
