__author__ = 'alexy'
import csv
from geopy.geocoders import GoogleV3

outfile = open('app/src/main/assets/Stores-coords.csv', 'w', newline='', encoding='utf-8')
geocoder = GoogleV3()
with open('app/src/main/assets/Stores.csv', newline='', encoding='utf-8') as csvfile:
    stores = csv.DictReader(csvfile, delimiter=';', quotechar='\"')
    fields = stores.fieldnames
    fields.append('lat')
    fields.append('lon')
    writer = csv.DictWriter(outfile, fieldnames=fields)
    writer.writeheader()
    for row in stores:
        if (row['Address'] and row['Address'] != 'unknown'):
            address = row['Address']
            if (row['City'] and row['City'] != 'unknown'):
                address += ',' + row['City']
            if (row['ZipCode'] and row['ZipCode'] != 'unknown'):
                address += ',' + row['ZipCode']
                address += ',Israel'
            location = geocoder.geocode(address)
            if (location):
                row['lat']=location.latitude 
                row['lon']=location.longitude
                writer.writerow(row)
            else:
                print('Geocode failed for '+ address)
        else:
            print ('Skipping '+row['StoreName'])
    outfile.close()



